module Homework

    implicit none
    contains
        subroutine FindMaxCoordinates(A, x1, y1, x2, y2)
        implicit none
        include "mpif.h"
        integer(4) :: mpiErr, mpiSize, mpiRank, task_step, task_finish, task_start, i, index, err
        real(8), intent(in), dimension(:,:) :: A
	real(8),dimension(:,:),allocatable :: B
        integer(4), intent(out) :: x1, y1, x2, y2
        integer(4) :: n, L, R, Up, Down, m, tmp, border
        real(8), allocatable :: current_column(:), max_sum, max_of_all_proc(:)
        real(8) :: current_sum, optimal, sum_of_sums 
        integer(4) :: X_1, Y_1, X_2, Y_2, maxindex
        logical :: transpos
        real(8) :: start, finish
        
      
        call mpi_comm_size(MPI_COMM_WORLD, mpiSize, mpiErr)
        call mpi_comm_rank(MPI_COMM_WORLD, mpiRank, mpiErr)
       
        allocate(B(n,m))
        allocate(current_column(m))
       	allocate(max_of_all_proc(mpiSize))       
        
        m = size(A, dim=1) 
        n = size(A, dim=2) 
        transpos = .FALSE.
        if (m < n) then 
            transpos = .TRUE.   
            B = transpose(A)
            m = size(B, dim=1) 
            n = size(B, dim=2) 
        else
            B = A     
        endif

        max_sum = B(1,1)
        x1=1
        y1=1
        x2=1
        y2=1

        do L= mpiRank+1, n, mpiSize
            current_column = B(:, L)            
            do R=L,n
                if (R > L) then 
                    current_column = current_column + B(:, R)
                endif
                call FindMaxInArray(current_column, current_sum, Up, Down) 
                if (current_sum > max_sum) then
                    max_sum = current_sum
                    x1 = Up
                    x2 = Down
                    y1 = L
                    y2 = R
                endif
            end do
        end do

	call mpi_gather(max_sum, 1, MPI_REAL8, max_of_all_proc, 1, MPI_REAL8, 0, MPI_COMM_WORLD, err)
	
	if (mpiRank==0) then
	    maxindex=maxloc(max_of_all_proc,dim=1)
	endif

        call mpi_bcast(maxindex, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, err)

        call mpi_bcast(x1, 1, MPI_INTEGER4, maxindex-1, MPI_COMM_WORLD, err)
        call mpi_bcast(x2, 1, MPI_INTEGER4, maxindex-1, MPI_COMM_WORLD, err)
        call mpi_bcast(y1, 1, MPI_INTEGER4, maxindex-1, MPI_COMM_WORLD, err)
        call mpi_bcast(y2, 1, MPI_INTEGER4, maxindex-1, MPI_COMM_WORLD, err)

        if (transpos) then  
            tmp = x1
            x1 = y1
            y1 = tmp
            tmp = y2
            y2 = x2
            x2 = tmp
        endif
       
        deallocate(current_column)
	deallocate(max_of_all_proc)
	deallocate(B)
        
    end subroutine

        subroutine FindMaxInArray(a, Sum, Up, Down)

            real(8), intent(in), dimension(:) :: a
            integer(4), intent(out) :: Up, Down
            real(8), intent(out) :: Sum
            real(8) :: cur_sum
            integer(4) :: minus_pos, i

            Sum = a(1)
            Up = 1
            Down = 1
            cur_sum = 0
            minus_pos = 0

            do i=1, size(a)
                cur_sum = cur_sum + a(i)
                if (cur_sum > Sum) then
                    Sum = cur_sum
                    Up = minus_pos + 1
                    Down = i
                endif
                if (cur_sum < 0) then
                    cur_sum = 0
                    minus_pos = i
                endif
            enddo
        end subroutine FindMaxInArray

end module Homework
